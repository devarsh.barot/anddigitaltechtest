package com.anddigitaltechtest.demo.Models;

public class CustomerResponse extends CustomerModel {

    private Long id;
    private String phoneNumber;

    public CustomerResponse(Long id, String phoneNumber) {
        super(id, phoneNumber);
        this.id = id;
        this.phoneNumber = phoneNumber;
    }

    public CustomerResponse(Long id, CustomerModel customerModel) {
        super(customerModel);
        this.id = id;
    }

    public CustomerResponse() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
