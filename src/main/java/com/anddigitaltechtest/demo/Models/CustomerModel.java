package com.anddigitaltechtest.demo.Models;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class CustomerModel {

    @Id
    @GeneratedValue
    private Long id;
    private String phoneNumber;

    public CustomerModel(Long id, String phoneNumber) {
        this.id = id;
        this.phoneNumber = phoneNumber;
    }

    public CustomerModel(CustomerModel customerModel) {
        this.id = customerModel.getId();
        this.phoneNumber = customerModel.getPhoneNumber();
    }

    public CustomerModel() {
        super();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
