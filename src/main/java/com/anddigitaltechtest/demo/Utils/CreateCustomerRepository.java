package com.anddigitaltechtest.demo.Utils;

import com.anddigitaltechtest.demo.Models.CustomerModel;
import com.anddigitaltechtest.demo.Models.CustomerResponse;
import com.anddigitaltechtest.demo.Repository.InformationRepo;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * In-memory repo that is used to create the initial data set. All data
 * is maintained/filtered through this class as long as the server is running
 * */

@Repository
public class CreateCustomerRepository implements InformationRepo {

    private Map<Long, CustomerResponse> customerMap;

    private CustomerCreateFactory customerFactory;

    public CreateCustomerRepository() {
        customerFactory = new CustomerCreateFactory();
    }

    @PostConstruct
    public void initialise() {
        customerMap = customerFactory.createInitialDataSet();
    }


    @Override
    public Collection<CustomerResponse> findAllCustomers() {
        Stream<CustomerResponse> customerStream = customerMap.entrySet().stream().map(Entry::getValue);
        return customerStream.collect(Collectors.toList());
    }

    @Override
    public Optional<CustomerResponse> findCustomerById(Long id) {
        return Optional.of(customerMap.get(id));
    }

    @Override
    public CustomerResponse addNewCustomer(CustomerModel customer) {
        return customerFactory.createNewCustomer(customer);
    }
}
