package com.anddigitaltechtest.demo.Utils;

import com.anddigitaltechtest.demo.Models.CustomerModel;
import com.anddigitaltechtest.demo.Models.CustomerResponse;

import java.util.Map;

public interface CustomerFactory {
    Map<Long, CustomerResponse> createInitialDataSet();

    CustomerResponse createNewCustomer(CustomerModel customerModel);
}
