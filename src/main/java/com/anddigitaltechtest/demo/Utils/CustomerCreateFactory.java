package com.anddigitaltechtest.demo.Utils;

import com.anddigitaltechtest.demo.Models.CustomerModel;
import com.anddigitaltechtest.demo.Models.CustomerResponse;

import java.util.HashMap;
import java.util.Map;

/*
 * Factory class to create multiple variations of customer data. Like the repository,
 * this class implements an interface.
 * */

public class CustomerCreateFactory implements CustomerFactory {

    private long id = 0;

    @Override
    public Map<Long, CustomerResponse> createInitialDataSet() {

        final HashMap<Long, CustomerResponse> customerMap = new HashMap<>();

        Long newId = incrementId();
        customerMap.put(newId, new CustomerResponse(newId, "93298328932"));

        newId = incrementId();
        customerMap.put(newId, new CustomerResponse(newId, "01234556778"));

        newId = incrementId();
        customerMap.put(newId, new CustomerResponse(newId, "32832929303"));

        return customerMap;
    }

    @Override
    public CustomerResponse createNewCustomer(CustomerModel customerToCreate) {
        return new CustomerResponse(incrementId(), customerToCreate.getPhoneNumber());
    }

    private Long incrementId() {
        return id++;
    }
}
