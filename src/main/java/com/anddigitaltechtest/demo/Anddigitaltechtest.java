package com.anddigitaltechtest.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Anddigitaltechtest {

    public static void main(String[] args) {
        SpringApplication.run(Anddigitaltechtest.class, args);
    }

}
