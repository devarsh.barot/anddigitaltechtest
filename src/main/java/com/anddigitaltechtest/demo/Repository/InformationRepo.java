package com.anddigitaltechtest.demo.Repository;

import com.anddigitaltechtest.demo.Models.CustomerModel;
import com.anddigitaltechtest.demo.Models.CustomerResponse;

import java.util.Collection;
import java.util.Optional;

/*
 * This interface is implemented to interact with the data layer/repository
 * for ease of use.
 * */

public interface InformationRepo {

    Collection<CustomerResponse> findAllCustomers();

    Optional<CustomerResponse> findCustomerById(Long id);

    CustomerResponse addNewCustomer(CustomerModel customer);

}
