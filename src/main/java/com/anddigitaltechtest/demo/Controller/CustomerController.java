package com.anddigitaltechtest.demo.Controller;

import com.anddigitaltechtest.demo.Models.CustomerModel;
import com.anddigitaltechtest.demo.Models.CustomerResponse;
import com.anddigitaltechtest.demo.Repository.InformationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

/*
 * Rest controller below maps the end points for specific things. Below, we can find the three
 * requested endpoints for activating a phone number, finding individual customers and finding
 * all customers. The base url mapping is /customer/.
 * */

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    private final InformationRepo informationRepo;

    @Autowired
    public CustomerController(InformationRepo informationRepo) {
        this.informationRepo = informationRepo;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public Optional<CustomerResponse> getCustomerById(@PathVariable("id") Long id) {
        return informationRepo.findCustomerById(id);
    }

    @GetMapping(value = "/get-all", produces = "application/json")
    public Collection<CustomerResponse> getContacts() {
        return informationRepo.findAllCustomers();
    }

    @PutMapping(value = "/create", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerResponse addContact(@RequestBody final CustomerModel customer) {
        return informationRepo.addNewCustomer(customer);
    }

}
