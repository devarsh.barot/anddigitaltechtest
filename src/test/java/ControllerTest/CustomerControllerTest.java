package ControllerTest;

import com.anddigitaltechtest.demo.Anddigitaltechtest;
import com.anddigitaltechtest.demo.Models.CustomerModel;
import com.anddigitaltechtest.demo.Utils.CreateCustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = Anddigitaltechtest.class)
public class CustomerControllerTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;
    private String baseUrl = "http://localhost:8080/customer";

    @Mock
    private CreateCustomerRepository customerCreationRepository;

    @Autowired
    private WebApplicationContext applicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
        customerCreationRepository.initialise();
    }

    @Test
    public void shouldRetrieveAllCustomersWhenEndpointIsCalled() throws Exception {

        mockMvc.perform(get(baseUrl + "/get-all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").exists())
                .andExpect(jsonPath("$[0].phoneNumber").exists())
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].phoneNumber").value("93298328932"));
    }

    @Test
    public void shouldRetrieveIndividualCustomerWhenIdProvided() throws Exception {
        mockMvc.perform(get(baseUrl + "/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.phoneNumber").exists())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.phoneNumber").value("01234556778"));
    }

    @Test
    public void shouldCreateCustomerWhenPhoneNumberIsProvided() throws Exception {
        CustomerModel newCustomer = new CustomerModel(1L, "0987654321");

        mockMvc.perform(put(baseUrl + "/create/")
                .contentType("application/json")
                .content(convertToJsonString(newCustomer)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.phoneNumber").exists())
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.phoneNumber").value("0987654321"));

    }

    private static String convertToJsonString(final Object object) throws IOException {
        return mapper.writeValueAsString(object);
    }

}
