# AND Digital technical test

This is the git project to demonstrate my code for the AND Digital technical test.

## Overview

The technical test is for a telecoms provider who require an API with 3 end-points for:

- Retrieving individual customer details
- Retrieving all customer details
- Activating a new phone number.

## Local Installation and run instructions

In order to run this application on your local machine. Simply follow the instructions below:

Clone the code using the following command

```
git clone https://gitlab.com/devarsh.barot/anddigitaltechtest
```

Change to the new directory

```
cd anddigitaltechtest
```

Change to develop branch

```
git checkout develop
```

Install dependencies

```
mvn clean install
```

Run the app

```
mvn spring-boot:run
```